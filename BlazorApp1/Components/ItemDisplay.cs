﻿namespace BlazorApp1.Components
{
    public interface ItemDisplay
    {
        String getTypeID();

        String getItem();

        int getNbElement();

        void changeState();

        int getIndex();

        void setNbElement(int nbElement);

        void setItem(Item item);
    }
}
