﻿using BlazorApp1.Modals;
using BlazorApp1.Sevices;
using Blazored.LocalStorage;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.JSInterop;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace BlazorApp1.Components
{
    public partial class Inventory
    {
        [Inject]
        public IStringLocalizer<Inventory> Localizer { get; set; }

        [Parameter]
        public List<Item> Items { get; set; }

        public Item CurrentDragItem { get; set; }

        public ItemDisplay CurrentEllement { get; set; }

        [Inject]
        public ILocalStorageService LocalStorage { get; set; }

        public List<String> InventoryItems { get; set; } = new List<String>() { null, null, null, null, null, null, null, null, null, null,
                                                                            null, null, null, null, null, null, null, null, null, null,
                                                                            null, null, null, null, null, null, null, null, null, null,
                                                                          };

        public List<int> InventoryNbElems { get; set; } = new List<int>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                          };

        public int CurrenttmpNbElem { get; set; }

        public ObservableCollection<InventoryAction> Actions { get; set; }

        [Inject]
        internal IJSRuntime JavaScriptRuntime { get; set; }

        public Inventory()
        {
            Actions = new ObservableCollection<InventoryAction>();
            Actions.CollectionChanged += OnActionsCollectionChanged;
        }

        public void StateChange()
        {
            StateHasChanged();
            LocalStorage.SetItemAsync("dataInv", InventoryItems);
            LocalStorage.SetItemAsync("dataNbInv", InventoryNbElems);
        }
        
        

        protected override async Task OnInitializedAsync()
        {
            try
            {
                var currentNumberData = await LocalStorage.GetItemAsync<List<int>>("dataNbInv");
                var currentData = await LocalStorage.GetItemAsync<List<String>>("dataInv");

                if (currentData != null)
                {
                    InventoryItems = await LocalStorage.GetItemAsync<List<String>>("dataInv");
                }
                else
                {
                    InventoryItems = new List<String>() { null, null, null, null, null, null, null, null, null, null,
                                                                            null, null, null, null, null, null, null, null, null, null,
                                                                            null, null, null, null, null, null, null, null, null, null,
                                                                          };
                }
                if (currentNumberData != null )
                { 
                    InventoryNbElems = await LocalStorage.GetItemAsync<List<int>>("dataNbInv");
                }
                else
                {
                    InventoryNbElems = new List<int>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                                                          };
                }
            }
            catch (Exception)
            {
            }
            
            StateHasChanged();
        }

        public Item getItem(String name)
        {
            int indice = 0;
            foreach (Item i in Items)
            {
                if (i.Name == name)
                {
                    return Items[indice];
                }
                indice++;
            }
            return null;
        }

        public String getNameCurrentItem()
        {
            if (CurrentDragItem != null) { 
                return CurrentDragItem.Name;
            }
            return null;
        }

        private void OnActionsCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
        {
            JavaScriptRuntime.InvokeVoidAsync("Inventory.AddActions", e.NewItems);
        }
    }
}
