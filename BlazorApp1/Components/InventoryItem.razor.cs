﻿using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Components
{
    public partial class InventoryItem : ItemDisplay
    {
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public Inventory Parent { get; set; }


        public void setItem(Item item)
        {
            Parent.InventoryItems[this.Index] = item.Name;
        }

        public void setNbElement(int nbElement)
        {
            Parent.InventoryNbElems[this.Index] = nbElement;
        }

        public void changeState()
        {
            StateHasChanged();
        }

        public String getItem()
        {
            return Parent.InventoryItems[this.Index];
        }

        public int getNbElement()
        {
            return Parent.InventoryNbElems[this.Index];
        }

        public String getTypeID()
        {
            return "InventoryItem";
        }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }

            Parent.Actions.Add(new InventoryAction { Action = "Drag Enter in Inventory", Item = this.Parent.getItem(this.Parent.InventoryItems[this.Index]), Index = this.Index, Nbelem = this.Parent.InventoryNbElems[this.Index] });
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }
            Parent.Actions.Add(new InventoryAction { Action = "Drag Leave in Inventory", Item = this.Parent.getItem(this.Parent.InventoryItems[this.Index]), Index = this.Index, Nbelem = this.Parent.InventoryNbElems[this.Index] });
        }

        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }

            if (this.Index == Parent.CurrentEllement.getIndex())
            {
                Parent.InventoryItems[this.Index] = Parent.getNameCurrentItem();
                Parent.InventoryNbElems[this.Index] = Parent.CurrenttmpNbElem;
            }
            else if (Parent.InventoryItems[this.Index] is not null)
            {
                if (Parent.InventoryItems[this.Index].Equals(Parent.getNameCurrentItem()))
                {
                    int StackSize =  Parent.getItem(Parent.InventoryItems[this.Index]).StackSize;

                    if (Parent.InventoryNbElems[this.Index] < StackSize && Parent.CurrentEllement.getTypeID() == "ListItem")
                    {
                        Parent.InventoryNbElems[this.Index]++;
                    }
                    else if (Parent.InventoryNbElems[this.Index] + Parent.CurrenttmpNbElem > StackSize && Parent.CurrentEllement.getTypeID() == "InventoryItem")
                    {
                        int tmp = Parent.InventoryNbElems[this.Index] + Parent.CurrenttmpNbElem;
                        Parent.InventoryNbElems[this.Index] = StackSize;
                        Parent.InventoryItems[Parent.CurrentEllement.getIndex()] = Parent.getNameCurrentItem();
                        Parent.InventoryNbElems[Parent.CurrentEllement.getIndex()] =tmp - StackSize;
                    }
                    else if (Parent.CurrentEllement.getTypeID() == "InventoryItem")
                    {
                        Parent.InventoryNbElems[this.Index] += Parent.CurrenttmpNbElem;
                        //Parent.CurrentEllement.setNbElement(0);
                        //Parent.CurrentEllement.setItem(null);
                    }
                    else
                    {
                        int indice = -1;
                        for (int i = 0; i < 40; i++)
                        {
                            if (Parent.InventoryItems[this.Index].Equals(Parent.InventoryItems[i]) && Parent.InventoryNbElems[i] < Parent.getItem(Parent.InventoryItems[i]).StackSize) {
                                Parent.InventoryNbElems[i]++;
                                indice = -1;
                                break;
                            }
                            else if (Parent.InventoryItems[i] is null && indice == -1)
                            {
                                indice = i;
                            }
                        }
                        if (indice != -1)
                        {
                            Parent.InventoryItems[indice] = Parent.InventoryItems[this.Index];
                            Parent.InventoryNbElems[indice] = 1;
                        }
                    }
                }
                else
                {
                    if (Parent.CurrentEllement.getTypeID() == "InventoryItem")
                    {
                        int tmp = Parent.InventoryNbElems[this.Index];
                        Parent.InventoryNbElems[this.Index] = Parent.CurrenttmpNbElem;
                        Parent.InventoryNbElems[Parent.CurrentEllement.getIndex()] = tmp;

                        String tmpI = Parent.InventoryItems[this.Index];
                        Parent.InventoryItems[this.Index] = Parent.getNameCurrentItem();
                        Parent.InventoryItems[Parent.CurrentEllement.getIndex()]=tmpI;
                    }
                }
            }
            else
            {
                String tmp = Parent.InventoryItems[this.Index];
                int tmpNb = Parent.InventoryNbElems[this.Index];

                Parent.InventoryItems[this.Index] = Parent.getNameCurrentItem();
                Parent.InventoryNbElems[this.Index] = Parent.CurrenttmpNbElem;

                if (Parent.CurrentEllement.getTypeID() == "InventoryItem")
                {
                    Parent.InventoryItems[Parent.CurrentEllement.getIndex()] = tmp;
                    Parent.InventoryNbElems[Parent.CurrentEllement.getIndex()] = tmpNb;

                }
                else if (Parent.CurrentEllement.getTypeID() == "ListItem")
                {

                }
            }
            Parent.CurrentEllement.changeState();
            StateHasChanged();
            Parent.StateChange();

            Parent.Actions.Add(new InventoryAction { Action = "Drag Drop in Inventory", Item = this.Parent.getItem(this.Parent.InventoryItems[this.Index]), Index = this.Index, Nbelem = this.Parent.InventoryNbElems[this.Index] });
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = Parent.getItem(Parent.InventoryItems[this.Index]);
            Parent.CurrentEllement = this;
            Parent.CurrenttmpNbElem = Parent.InventoryNbElems[this.Index];
            Parent.Actions.Add(new InventoryAction { Action = "Drag Enter in Inventory", Item = this.Parent.getItem(this.Parent.InventoryItems[this.Index]), Index = this.Index, Nbelem = this.Parent.InventoryNbElems[this.Index] });

            Parent.InventoryItems[this.Index] = null;
            Parent.InventoryNbElems[this.Index] = 0;
        }

        public int getIndex()
        {
            return Index;
        }
    }

}
