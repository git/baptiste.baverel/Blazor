﻿window.Inventory =
{
    AddActions: function (data) {

        data.forEach(element => {
            var div = document.createElement('div');
            div.innerHTML = 'Action: ' + element.action + ' - Index: ' + element.index;

            if (element.item) {
                div.innerHTML += ' - Item Name: ' + element.item.name + ' - Nb Element : ' + element.nbelem ;
            }

            document.getElementById('actionsinv').appendChild(div);
        });
    }
}