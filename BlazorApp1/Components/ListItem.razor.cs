﻿using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Components
{
    public partial class ListItem :ItemDisplay
    {
        [Parameter]
        public int Index { get; set; }

        [Parameter]
        public Item Item { get; set; }

        [Parameter]
        public bool NoDrop { get; set; }

        [CascadingParameter]
        public Inventory Parent { get; set; }

        public void setItem(Item item) { }

        public void setNbElement(int nbElement) { }

        public void changeState()
        {
            StateHasChanged();
        }

        public String getItem()
        {
            return Item.Name;
        }

        public int getNbElement()
        {
            return 1;
        }

        public String getTypeID()
        {
            return "ListItem";
        }

        internal void OnDragEnter()
        {
            if (NoDrop)
            {
                return;
            }
            Parent.Actions.Add(new InventoryAction { Action = "Drag Enter in Items List", Item = this.Item, Index = this.Index, Nbelem = 1 });
        }

        internal void OnDragLeave()
        {
            if (NoDrop)
            {
                return;
            }
            Parent.Actions.Add(new InventoryAction { Action = "Drag Leave in Items List", Item = this.Item, Index = this.Index, Nbelem = 1 });
        }

        internal void OnDrop()
        {
            if (NoDrop)
            {
                return;
            }
            Parent.Actions.Add(new InventoryAction { Action = "Drag Drop in Items List", Item = this.Item, Index = this.Index, Nbelem = 1 });
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = this.Item;
            Parent.CurrentEllement = this;
            Parent.CurrenttmpNbElem = 1;
            Parent.Actions.Add(new InventoryAction { Action = "Drag Start in Items List", Item = this.Item, Index = this.Index, Nbelem = 1 });
        }

        public int getIndex()
        {
            return -1;
        }
    }
}
