﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlazorApp1.Models
{
    public class Cake
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Cost { get; set; }
    }
}