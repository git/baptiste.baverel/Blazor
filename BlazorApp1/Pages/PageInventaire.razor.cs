﻿using BlazorApp1.Components;
using BlazorApp1.Models;
using BlazorApp1.Sevices;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace BlazorApp1.Pages
{
    public partial class PageInventaire
    {
        [Inject]
        public IDataService DataService { get; set; }

        [Inject]
        public IStringLocalizer<PageInventaire> Localizer { get; set; }

        public List<Item> Items { get; set; } = new List<Item>();



        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            base.OnAfterRenderAsync(firstRender);

            if (!firstRender)
            {
                return;
            }

            Items = await DataService.List(0, await DataService.Count());
            

            StateHasChanged();
        }
    }
    
}
